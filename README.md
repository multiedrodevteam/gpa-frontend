# OVERVIEW DO PROJETO #

Esta aplicação será utilizada por funcionários da Latam em diversos países nas Américas e Europa para cadastrarem dados 
referentes a práticas, ausências e empoderamentos. Em posse destes dados, os gestores utilizam as informações geradas pelos
Dashboards.

# LINK PARA DOCUMENTAÇÃO #
em construção ...

# ARQUITETURA E TECNOLOGIAS #

1. Google Cloud Plataform
* App Engine
* Cloud SQL
2. Linguagens e frameworks
* Java
* Hibernate
* JAX-RS (implementação Jersey)

# AMBIENTES #
  
Para alterar o BD da aplicação, comente/descomente a URL nos seguintes arquivos:  
  
* src/Login  
* src/components/Home  
* src/components/relatorios/Praticas  
* src/components/Usuario/CadastroNaoEspecialista  
* src/components/Usuario/Usuarios  
* src/vuex/actions  

# TIME #

* Thiago Melo Fontana
* Vinicius Melo
* Adriana Durante
* Bruno Morgan